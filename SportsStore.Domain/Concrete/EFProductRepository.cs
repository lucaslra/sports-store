﻿using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using System.Collections.Generic;

namespace SportsStore.Domain.Concrete
{
    public class EFProductRepository : IProductRepository
    {
        private readonly EFDbContext _context = new EFDbContext();

        public IEnumerable<Product> Products
        {
            get { return _context.Products; }
        }

        public void Save(Product product)
        {
            if (product.ProductId == 0)
            {
                _context.Products.Add(product);
            }
            else
            {
                Product currentProduct = _context.Products.Find(product.ProductId);
                if (currentProduct != null)
                {
                    currentProduct.Name = product.Name;
                    currentProduct.Description = product.Description;
                    currentProduct.Category = product.Category;
                    currentProduct.Price = product.Price;

                    currentProduct.ImageData = product.ImageData;
                    currentProduct.ImageMimeType = product.ImageMimeType;
                }
            }
            _context.SaveChanges();
        }

        public Product Delete(Product product)
        {
            Product currentProduct = _context.Products.Find(product.ProductId);
            if (currentProduct != null)
            {
                _context.Products.Remove(currentProduct);
            }
            _context.SaveChanges();

            return currentProduct;
        }
    }
}