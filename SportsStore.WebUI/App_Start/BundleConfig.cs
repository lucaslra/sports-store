﻿using System.Web.Optimization;

namespace SportsStore.WebUI
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/Bootstrap").Include("~/Content/bootstrap.css")
                .Include("~/Content/bootstrap-theme.css"));

            bundles.Add(new StyleBundle("~/Content/Site").Include("~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Content/ErrorStyles").Include("~/Content/ErrorStyles.css"));

            bundles.Add(new ScriptBundle("~/Scripts/jQuery").Include("~/Scripts/jquery-{version}.js"));
            bundles.Add(new ScriptBundle("~/Scripts/jQueryValidation").Include("~/Scripts/jquery.validate.js")
                .Include("~/Scripts/jquery.validate.unobtrusive.js")
                .Include("~/Scripts/jquery.validate.decimal.js"));

            bundles.Add(new ScriptBundle("~/Scripts/Bootstrap").Include("~/Scripts/bootstrap.js"));

            BundleTable.EnableOptimizations = true;
        }
    }
}