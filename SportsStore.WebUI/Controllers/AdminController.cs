﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;

namespace SportsStore.WebUI.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private readonly IProductRepository _productRepository;

        public AdminController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public ViewResult Index()
        {
            return View(_productRepository.Products);
        }

        public ViewResult Edit(int productId)
        {
            Product product = _productRepository.Products.FirstOrDefault(p => p.ProductId == productId);

            return View(product);
        }

        public ViewResult Create()
        {
            return View("Edit", new Product());
        }

        public ViewResult Delete(int productId)
        {
            Product product = _productRepository.Products.FirstOrDefault(p => p.ProductId == productId);

            return View(product);
        }

        [HttpPost]
        public ActionResult Delete(Product product)
        {
            Product deletedProduct = _productRepository.Delete(product);
            if (deletedProduct != null)
            {
                TempData["message"] = string.Format("{0} was deleted", deletedProduct.Name);
            } return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(Product product, HttpPostedFileBase imageFile)
        {
            if (!ModelState.IsValid) return View(product);

            if (imageFile != null)
            {
                product.ImageMimeType = imageFile.ContentType;
                product.ImageData = new byte[imageFile.ContentLength];
                imageFile.InputStream.Read(product.ImageData, 0, imageFile.ContentLength);
            }

            _productRepository.Save(product);

            TempData["message"] = string.Format("{0} has been saved.", product.Name);

            return RedirectToAction("Index");
        }
    }
}