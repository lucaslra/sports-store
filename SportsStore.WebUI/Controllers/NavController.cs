﻿using System.Linq;
using System.Web.Mvc;
using SportsStore.Domain.Abstract;
using SportsStore.WebUI.Models;

namespace SportsStore.WebUI.Controllers
{
    public class NavController : Controller
    {
        private readonly IProductRepository _repository;

        public NavController(IProductRepository repository)
        {
            _repository = repository;
        }

        public PartialViewResult Menu(string category = null, bool horizontalLayout = false)
        {
            NavViewModel model = new NavViewModel
            {
                Categories = _repository.Products.Select(p => p.Category).Distinct().OrderBy(p => p),
                SelectedCategory = category
            };
            return PartialView("Menu", model);
        }
    }
}