﻿using System;
using System.Web.Configuration;
using SportsStore.Domain.Abstract;
using System.Linq;
using System.Web.Mvc;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Models;

namespace SportsStore.WebUI.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductRepository _repository;
        public int PageSize = 4;

        public ProductController(IProductRepository repository)
        {
            _repository = repository;
        }

        public ViewResult List(int page = 1, string category = "")
        {
            ProductsListViewModel model = new ProductsListViewModel
            {
                Products = _repository.Products
                    .Where(p => String.Equals(p.Category, category, StringComparison.CurrentCultureIgnoreCase) || string.IsNullOrEmpty(category))
                    .OrderBy(p => p.ProductId)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = _repository.Products.Count(p => String.Equals(p.Category, category, StringComparison.CurrentCultureIgnoreCase) || string.IsNullOrEmpty(category))
                },
                CurrentCategory = category,
                CategoryList = _repository.Products.Select(p => p.Category).Distinct().ToList()
            };

            return View(model);
        }

        public FileContentResult GetImage(int productId)
        {
            Product product = _repository.Products.FirstOrDefault(p => p.ProductId == productId);

            return product != null ? File(product.ImageData, product.ImageMimeType) : null;
        }
    }
}