﻿using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Controllers;
using SportsStore.WebUI.Models;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class CartControllerTests
    {
        [TestMethod]
        public void Can_Add_Items_to_Cart()
        {
            // Arrange
            Mock<IProductRepository> mock = GetMockIProductRepository;
            Mock<IOrderProcessor> mock2 = GetMockIOrderProcessor;

            // Arrange
            Cart cart = new Cart();
            CartController controller = new CartController(mock.Object, mock2.Object);

            // Act
            controller.AddToCart(cart, 1, null);

            // Assert
            Assert.AreEqual(1, cart.Lines.Count());
            Assert.AreEqual(1, cart.Lines.ToArray()[0].Product.ProductId);
        }

        private static Mock<IProductRepository> GetMockIProductRepository
        {
            get
            {
                Mock<IProductRepository> mock = new Mock<IProductRepository>();
                mock.Setup(m => m.Products).Returns(new[]
                {
                    new Product {ProductId = 1, Name = "P1", Category = "Apples"}
                }.AsQueryable());
                return mock;
            }
        }
        private static Mock<IOrderProcessor> GetMockIOrderProcessor
        {
            get
            {
                Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
                mock.Setup(p => p.ProcessOrder(new Cart(), new ShippingDetails()));
                return mock;
            }
        }

        [TestMethod]
        public void User_is_Redirected_to_Index()
        {
            // Arrange
            Mock<IProductRepository> mock = GetMockIProductRepository;
            Mock<IOrderProcessor> mock2 = GetMockIOrderProcessor;

            Cart cart = new Cart();
            CartController controller = new CartController(mock.Object, mock2.Object);

            // Act
            RedirectToRouteResult result = controller.AddToCart(cart, 1, "returnUrl");

            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("returnUrl", result.RouteValues["returnUrl"]);
        }

        [TestMethod]
        public void Can_View_Cart_Contents()
        {
            //  Arrange - create a Cart
            Cart cart = new Cart();
            //  Arrange - create the controller
            CartController target = new CartController(null, null);
            //  Act - call the Index action me thod
            CartIndexViewModel result = (CartIndexViewModel)target.Index(cart, "myUrl").ViewData.Model;
            //  Assert
            Assert.AreSame(result.Cart, cart);
            Assert.AreEqual(result.ReturnUrl, "myUrl");
        }

        [TestMethod]
        public void Cannot_Checkout_Empty_Cart()
        {
            Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
            Cart cart = new Cart();
            ShippingDetails shipping = new ShippingDetails();

            CartController target = new CartController(null, mock.Object);

            ViewResult result = target.Checkout(cart, shipping);

            mock.Verify(m => m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Never());

            Assert.AreEqual("", result.ViewName);
            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Cannot_Checkout_Invalid_ShippingDetails()
        {
            Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
            Cart cart = new Cart();
            cart.AddItem(new Product(), 1);

            CartController target = new CartController(null, mock.Object);

            target.ModelState.AddModelError("error", "error");

            ViewResult result = target.Checkout(cart, new ShippingDetails());

            mock.Verify(p => p.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Never());

            Assert.AreEqual("", result.ViewName);
            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Can_Checkout_And_Submit_Order()
        {
            Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
            Cart cart = new Cart();
            cart.AddItem(new Product(), 1);

            CartController target = new CartController(null, mock.Object);

            ViewResult result = target.Checkout(cart, new ShippingDetails());

            mock.Verify(m => m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Once());

            Assert.AreEqual("Completed", result.ViewName);
            Assert.AreEqual(true, result.ViewData.ModelState.IsValid);
        }
    }
}
