﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportsStore.Domain.Entities;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class CartTests
    {
        [TestMethod]
        public void Can_Add_New_Lines()
        {
            // Arrange
            Cart cart = new Cart();
            Product p = new Product
            {
                Name = "Nexus 5",
                Category = "Mobile",
                Description = "Google Mobile Phone",
                Price = 1200,
                ProductId = 1
            };
            Product p2 = new Product
            {
                Name = "Iphone 6",
                Category = "Mobile",
                Description = "Apple Mobile Phone",
                Price = 2000,
                ProductId = 2
            };

            // Act
            cart.AddItem(p, 2);
            cart.AddItem(p2, 1);
            CartLine[] result = cart.Lines.ToArray();


            // Assert
            Assert.AreEqual(2, result.Count());
            Assert.AreEqual(p, result[0].Product);
            Assert.AreEqual(p2, result[1].Product);
        }

        [TestMethod]
        public void Can_Add_Quantity_For_Existing_Lines()
        {
            // Arrange
            Cart cart = new Cart();
            Product p = new Product
            {
                Name = "Nexus 5",
                Category = "Mobile",
                Description = "Google Mobile Phone",
                Price = 1200,
                ProductId = 1
            };

            // Act
            cart.AddItem(p, 1);
            var result = cart.Lines.ToArray();

            // First Assert
            Assert.AreEqual(p, result[0].Product);
            Assert.AreEqual(1, result[0].Quantity);
            Assert.AreEqual(1, result.Count());

            // Act
            cart.AddItem(p, 9);
            result = cart.Lines.ToArray();

            // Second Assert
            Assert.AreEqual(p, result[0].Product);
            Assert.AreEqual(10, result[0].Quantity);
            Assert.AreEqual(1, result.Count());

        }

        [TestMethod]
        public void Can_Remove_Line()
        {
            // Arrange
            Cart cart = new Cart();
            Product p = new Product
            {
                Name = "Nexus 5",
                Category = "Mobile",
                Description = "Google Mobile Phone",
                Price = 1200,
                ProductId = 1
            };
            Product p2 = new Product
            {
                Name = "Iphone 6",
                Category = "Mobile",
                Description = "Apple Mobile Phone",
                Price = 2000,
                ProductId = 2
            };

            // Act
            cart.AddItem(p, 2);
            cart.AddItem(p2, 1);
            CartLine[] result = cart.Lines.ToArray();

            // First Assert
            Assert.AreEqual(2, result.Length);
            Assert.AreEqual(p, result[0].Product);
            Assert.AreEqual(p2, result[1].Product);

            // Act
            cart.RemoveLine(p);
            result = cart.Lines.ToArray();

            // Second Assert
            Assert.AreEqual(1, result.Length);
            Assert.AreEqual(p2, result[0].Product);
        }

        [TestMethod]
        public void Calculate_Cart_Total()
        {
            // Arrange - create some test products
            Product p1 = new Product { ProductId = 1, Name = "P1", Price = 100M };
            Product p2 = new Product { ProductId = 2, Name = "P2", Price = 50M };

            // Act
            Cart cart = new Cart();
            cart.AddItem(p1, 10);
            cart.AddItem(p2, 5);

            // Assert
            Assert.AreEqual(1250M, cart.ComputeTotalValue());
        }

        [TestMethod]
        public void Can_Clear_Contents()
        {
            // Arrange 
            Product p1 = new Product { ProductId = 1, Name = "P1", Price = 100M };
            Product p2 = new Product { ProductId = 2, Name = "P2", Price = 50M };
            Cart cart = new Cart();
            cart.AddItem(p1, 2);
            cart.AddItem(p2, 2);

            // Act
            cart.Clear();

            // Assert
            Assert.AreEqual(cart.Lines.Count(), 0);
        }
    }
}
