﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Controllers;
using SportsStore.WebUI.Models;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class NavControllerTests
    {
        [TestMethod]
        public void Can_Create_Categories()
        {
            // Arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new[]
            {
                new Product {ProductId = 1, Name = "P1", Category = "Apples"},
                new Product {ProductId = 2, Name = "P2", Category = "Apples"},
                new Product {ProductId = 3, Name = "P3", Category = "Plums"},
                new Product {ProductId = 4, Name = "P4", Category = "Oranges"}
            });

            NavController controller = new NavController(mock.Object);

            // Act
            string[] categories = (((NavViewModel)controller.Menu().Model).Categories.ToArray());

            // Assert
            Assert.AreEqual(categories.Length, 3);
            Assert.AreEqual(categories[0], "Apples");
            Assert.AreEqual(categories[1], "Oranges");
            Assert.AreEqual(categories[2], "Plums");
        }

        [TestMethod]
        public void Indicates_Selected_Category()
        {
            // Assert
            // Arrange
            // - create the mock repository
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product {ProductId = 1, Name = "P1", Category = "Apples"},
                new Product {ProductId = 4, Name = "P2", Category = "Oranges"},
            });

            NavController controller = new NavController(mock.Object);

            // Act
            var results = ((NavViewModel)controller.Menu("Apples").Model);

            // Arrange
            Assert.AreEqual("Apples", results.SelectedCategory);
        }
    }
}
