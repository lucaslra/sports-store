﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Controllers;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class AdminControllerTests
    {
        [TestMethod]
        public void Index_Contains_All_Products()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new[]
            {
                new Product {Name = "P1", ProductId = 1},
                new Product {Name = "P2", ProductId = 2},
                new Product {Name = "P3", ProductId = 3}
            });

            AdminController target = new AdminController(mock.Object);

            var result = ((IEnumerable<Product>)target.Index().ViewData.Model).ToArray();

            Assert.AreEqual(3, result.Length);
            Assert.AreEqual("P1", result[0].Name);
            Assert.AreEqual("P2", result[1].Name);
            Assert.AreEqual("P3", result[2].Name);
        }

        [TestMethod]
        public void Can_Edit_Product()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new[]
            {
                new Product {Name = "P1", ProductId = 1},
                new Product {Name = "P2", ProductId = 2},
                new Product {Name = "P3", ProductId = 3}
            });

            AdminController target = new AdminController(mock.Object);

            var result = ((Product)target.Edit(2).ViewData.Model);

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.ProductId);
            Assert.AreEqual("P2", result.Name);
        }

        [TestMethod]
        public void Cannot_edit_NonExistent_Product()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new[]
            {
                new Product {Name = "P1", ProductId = 1},
                new Product {Name = "P2", ProductId = 2},
                new Product {Name = "P3", ProductId = 3}
            });

            AdminController target = new AdminController(mock.Object);

            var result = ((Product)target.Edit(5).ViewData.Model);

            Assert.IsNull(result);
        }

        [TestMethod]
        public void Can_Save_Valid_Changes()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            AdminController controller = new AdminController(mock.Object);

            Product product = new Product { Name = "Test" };

            ActionResult result = controller.Edit(product, null);

            mock.Verify(c => c.Save(product));

            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Cannot_Save_Invalid_Changes()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            AdminController controller = new AdminController(mock.Object);

            Product product = new Product { Name = "Test" };

            controller.ModelState.AddModelError("error", "error");

            ActionResult result = controller.Edit(product, null);

            mock.Verify(c => c.Save(It.IsAny<Product>()), Times.Never());

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Can_Delete_Valid_Products()
        {
            var p1 = new Product { Name = "P1", ProductId = 1 };
            var p2 = new Product { Name = "P2", ProductId = 2 };
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(p => p.Products).Returns(new[] { p1, p2 });

            AdminController controller = new AdminController(mock.Object);

            controller.Delete(p1);

            mock.Verify(o => o.Delete(p1));
        }
    }
}
