﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportsStore.WebUI.Models;
using HtmlHelper = System.Web.Mvc.HtmlHelper;
using SportsStore.WebUI.HtmlHelpers;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class HtmlHelpersTest
    {
        [TestMethod]
        public void Can_Generate_Page_Links()
        {
            //Arrange
            HtmlHelper myHelper = null;


            PagingInfo paging = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };
            Func<int, string> pageUrlDelegate = i => "Page " + i;

            // act 
            // ReSharper disable ExpressionIsAlwaysNull
            MvcHtmlString result = myHelper.PageLinks(paging, pageUrlDelegate);
            // ReSharper restore ExpressionIsAlwaysNull


            // Assert
            Assert.AreEqual(@"<a class=""btn btn-default"" href=""Page 1"">1</a>" +
                            @"<a class=""btn btn-default btn-primary selected"" href=""Page 2"">2</a>" +
                            @"<a class=""btn btn-default"" href=""Page 3"">3</a>", result.ToString());
        }
    }
}
